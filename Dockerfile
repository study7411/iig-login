FROM node:16-alpine

WORKDIR /app
COPY package.json /app/package.json

RUN apk add --no-cache make gcc g++ python3 && \
  npm install && \
  npm rebuild bcrypt --build-from-source && \
  apk del make gcc g++ python3

COPY . /app

EXPOSE 3000
USER node      
CMD ["npm", "start"]
