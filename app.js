import express from "express";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import multer from "multer";
import dotenv from "dotenv";
import mongoose from "mongoose";
import { dirname, resolve } from "path";
import { fileURLToPath } from "url";
import login from "./routes/login.js";
import users from "./routes/users.js";

var app = express();
var forms = multer();
dotenv.config();
const __dirname = dirname(fileURLToPath(import.meta.url));

mongoose.Promise = global.Promise;
mongoose.connect(
  process.env.DATABASE_URL, // DATABASE_URL
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (!err) console.log("MongoDB has connected successfully.");
  }
);

app.use(cookieParser());
app.use(bodyParser.json());
app.use(forms.array());
app.use(bodyParser.urlencoded({ extended: true }));

// routes api
app.use("/", login);
app.use("/user", users);

// routes frt
app.get("*", function (_, res) {
  res.sendFile(resolve(__dirname, "index.html"));
});

app.listen(3000, () => console.log("server run listening on port 3000"));
