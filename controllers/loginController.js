import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import User from "../models/userModel.js";

export const login = (req, res) => {
  // find user by username in query
  User.findOne({ username: req.query.username }, async (err, user) => {
    if (err) {
      res.status(400).send({
        status: "error",
        message: err.message,
      });
    }
    if (user) {
      // check password
      const validPassword = await bcrypt.compare(
        req.query.password,
        user.password
      );
      if (!validPassword) {
        res.status(400).send({
          status: "error",
          message: "Invalid Password!",
        });
      }
      // payload in token
      const payload = {
        _id: user._id,
      };
      // sign token
      jwt.sign(
        payload,
        process.env.SECRET_KEY, // SECRET_KEY
        {
          expiresIn: "1h", // expires in 1 hour
        },
        (_, token) => {
          res
            .cookie("access_token", token, {
              httpOnly: true,
              secure: process.env.NODE_ENV === "production",
            }) // set access_token in cookie
            .send({
              status: "ok",
              message: "Logged in successfully",
            });
        }
      );
    } else {
      res.status(400).send({
        status: "error",
        message: "Invalid Username!",
      });
    }
  });
};

export const logout = (_, res) => {
  return res.clearCookie("access_token").send({
    status: "ok",
    message: "Successfully logged out",
  }); // clear access_token in cookie
};
