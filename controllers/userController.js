import bcrypt from "bcrypt";
import User from "../models/userModel.js";

export const addNewUser = async (req, res) => {
  // hash password
  const salt = await bcrypt.genSalt(10);
  req.body.password = await bcrypt.hash(req.body.password, salt);
  // new user
  const newUser = new User(req.body);
  // save new user
  newUser.save((err, user) => {
    if (err) {
      res.status(400).send({
        status: "error",
        message: err.message,
      });
    } else {
      res.status(201).send({
        status: "ok",
        user,
        message: "Created in successfully",
      });
    }
  });
};

export const getUser = (req, res) => {
  // find user by _id in token
  User.findOne({ _id: req._id }, (err, user) => {
    if (err) {
      res.status(400).send({
        status: "error",
        message: err.message,
      });
    } else {
      res.status(200).send({
        status: "ok",
        user,
        message: "",
      });
    }
  });
};

export const getUserByUsername = (req, res) => {
  // find user by username in params
  User.findOne({ username: req.params.username }, (err, user) => {
    if (err) {
      res.status(400).send({
        status: "error",
        message: err.message,
      });
    } else {
      res.status(200).send({
        status: "ok",
        user,
        message: "",
      });
    }
  });
};

export const updateUserByID = async (req, res) => {
  // find user by _id in token
  User.findOne({ _id: req._id }, async (err, user) => {
    if (err) {
      res.status(400).send({
        status: "error",
        message: err.message,
      });
    } else {
      let notChange = false;
      let match = false;
      if (req.body.password) {
        // check password not change
        const validPassword = await bcrypt.compare(
          req.body.password,
          user.password
        );
        if (validPassword) {
          notChange = true;
        }
        // check last password
        for (const password of user.lastPassword) {
          const validPassword = await bcrypt.compare(
            req.body.password,
            password
          );
          if (validPassword) {
            match = true;
            break;
          }
        }
      }
      if (notChange) {
        res.status(400).send({
          status: "error",
          message: "Old password!",
        });
      } else if (match) {
        res.status(400).send({
          status: "error",
          message: "This password has been used before!",
        });
      } else {
        if (req.body.password) {
          // hash password
          const salt = await bcrypt.genSalt(10);
          req.body.password = await bcrypt.hash(req.body.password, salt);
          // edit last password
          req.body.lastPassword = user.lastPassword;
          if (user.lastPassword.length >= 5) {
            req.body.lastPassword = user.lastPassword.slice(-4);
          }
          req.body.lastPassword.push(user.password);
        }
        // update user by _id in token
        User.findByIdAndUpdate(req._id, req.body, (err, _) => {
          if (err) {
            res.status(400).send({
              status: "error",
              message: err.message,
            });
          } else {
            // find user
            User.findOne({ _id: req._id }, (_, user) => {
              res.status(200).send({
                status: "ok",
                user,
                message: "Edited in successfully",
              });
            });
          }
        });
      }
    }
  });
};
