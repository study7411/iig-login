import jwt from "jsonwebtoken";

export const authorization = (req, res, next) => {
  const token = req.cookies.access_token;
  if (!token) {
    return res.status(403).send({
      status: "error",
      message: "Access Token not Found!",
    });
  }
  try {
    const data = jwt.verify(token, process.env.SECRET_KEY); // SECRET_KEY
    req._id = data._id; // set _id in req
    return next();
  } catch {
    return res.status(403).send({
      status: "error",
      message: "Access Token not Working!",
    });
  }
};
