import mongoose from "mongoose";

var UserSchema = mongoose.Schema(
  {
    username: {
      type: String,
      required: "Enter username",
      unique: true,
    },
    password: {
      type: String,
      required: "Enter password",
    },
    firstname: {
      type: String,
      required: "Enter first name",
    },
    lastname: {
      type: String,
    },
    image: {
      type: String,
      required: "Enter image",
    },
    lastPassword: [
      {
        type: String,
      },
    ],
    created_date: {
      type: Date,
      default: Date.now(),
    },
    updated_date: {
      type: Date,
      default: Date.now(),
    },
  },
  { collection: "users" }
);

var User = mongoose.model("User", UserSchema);

export default User;
