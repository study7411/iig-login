import { Router } from "express";
import { login, logout } from "../controllers/loginController.js";

var router = Router();

router.get("/login", login);
router.get("/logout", logout);

export default router;
