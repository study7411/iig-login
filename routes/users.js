import { Router } from "express";
import {
  addNewUser,
  updateUserByID,
  getUser,
  getUserByUsername,
} from "../controllers/userController.js";
import { authorization } from "../middleware/token.js";

var router = Router();

router.post("/", addNewUser);
router.put("/", authorization, updateUserByID);
router.get("/", authorization, getUser);
router.get("/:username", getUserByUsername);

export default router;
